<h2 class="page-header">Tweets</h2>
<div class="row">
    @if(count($tweets) === 0)
        <h3>No tweets to display</h3>
    @else
        @foreach($tweets as $tweet)
            <div class="col-md-4">
                @include('mixins.tweet')
            </div>
        @endforeach
    @endif
</div>