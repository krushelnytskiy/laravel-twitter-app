<div class="media">
    <div class="media-left">
        <div class="media-object">
            <img src="{{$tweet->user_photo}}" alt="">
        </div>
    </div>
    <div class="media-body">
        <h4 class="media-heading">{{ $tweet->user_name }}</h4>
        <p>
            {{ $tweet->tweet_text }}
        </p>
        <div class="page-header">
            <a href="{{ $tweet->tweet_url }}">View on Twitter</a>
        </div>
    </div>
</div>