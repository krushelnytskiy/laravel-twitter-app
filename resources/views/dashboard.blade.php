@extends('layout')
@section('content')
    @include('mixins.tweets')
@endsection
@section('scripts')
    <script>
        setInterval(
            function () {
                jQuery.ajax(
                    {
                        url: '/tweets',
                        success: function (data) {
                            if (data.hasOwnProperty('html')) {
                                jQuery('#app').html(data.html);
                            }
                        }
                    }
                );
            },
            5000
        );
    </script>
@endsection