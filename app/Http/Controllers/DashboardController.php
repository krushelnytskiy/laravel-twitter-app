<?php

namespace App\Http\Controllers;

use App\Tweet;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    public function index()
    {
        $tweets = Tweet::all();

        return view('dashboard', [
            'tweets' => $tweets
        ]);
    }

    public function tweets()
    {
        $tweets = Tweet::all();

        return [
            'html' => view(
                'mixins.tweets',
                [
                    'tweets'   => $tweets
                ]
            )->render()];
    }
}