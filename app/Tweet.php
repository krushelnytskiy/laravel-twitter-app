<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tweet
 * @package App
 */
class Tweet extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['tweet_url', 'tweet_text', 'user_id', 'user_name', 'user_location', 'user_photo', 'created'];

    /**
     * @var string
     */
    protected $table = 'tweets';

}
