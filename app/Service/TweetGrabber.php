<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Psr\Http\Message\StreamInterface;

/**
 * Class TweetGrabber
 * @package App\Service
 */
class TweetGrabber
{

    /**
     * Api constants
     */
    const BASE_API_URL = 'https://stream.twitter.com/1.1/';
    const METHOD_STATUSES_FILTER = 'statuses/filter.json';

    /**
     * @return StreamInterface
     */
    public function getStreamingBody()
    {
        $stack = HandlerStack::create();

        $stack->push(
            new Oauth1(
                [
                    'consumer_key'    => env('TWITTER_CONSUMER_KEY'),
                    'consumer_secret' => env('TWITTER_CONSUMER_SECRET'),
                    'token'           => env('TWITTER_ACCESS_TOKEN'),
                    'token_secret'    => env('TWITTER_ACCESS_TOKEN_SECRET'),
                ]
            )
        );

        $client = new Client(
            [
                'base_uri' => static::BASE_API_URL,
                'handler' => $stack,
                'auth' => 'oauth',
                'stream' => true,
            ]
        );

        $response = $client->post(
            static::METHOD_STATUSES_FILTER,
            [
                'form_params' => [
                    'follow' => env('TWITTER_ACCOUNT_FOLLOW'),
                ],
            ]
        );

        return $response->getBody();
    }

    /**
     * @param StreamInterface $body
     * @return string
     */
    public function line($body)
    {
        $line = '';
        while (false === $body->eof()) {
            if (false === ($byte = $body->read(1))) {
                return $line;
            }

            $line .= $byte;

            if (substr($line, -strlen(PHP_EOL)) === PHP_EOL) {
                break;
            }
        }

        return $line;
    }

}