<?php

namespace App\Console\Commands;

use App\Service\TweetGrabber;
use App\Tweet;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class TwitterGrab
 * @package App\Console\Commands
 */
class TwitterGrab extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitter:grabber';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start';

    /**
     * @var TweetGrabber
     */
    protected $twitterGrabberService;

    public function __construct()
    {
        $this->twitterGrabberService = new TweetGrabber();

        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $body = $this->twitterGrabberService->getStreamingBody();

        while(false === $body->eof()) {
            $tweet = json_decode($this->twitterGrabberService->line($body), true);

            if (null !== $tweet && array_key_exists('text', $tweet)) {

                $user = $tweet['user'];

                Tweet::create(
                    [
                        'tweet_url' => implode(
                            '/',
                            [
                                'https://twitter.com',
                                $user['screen_name'],
                                'status',
                                $tweet['id_str']
                            ]
                        ),
                        'tweet_text'    => $tweet['text'],
                        'user_id'       => $user['id_str'],
                        'user_name'     => $user['name'],
                        'user_location' => $user['location'],
                        'user_photo'    => $user['profile_image_url'],
                        'content'       => $tweet['text']
                    ]
                );
            }
        }
    }
}
